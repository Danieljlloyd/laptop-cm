#!/bin/bash

install_pkg_count=0
install_fail_count=0
install_success_count=0
function install_pkg
{
	install_pkg_count=$((install_pkg_count+1))

	sudo apt-get -qy install $@

	if [ $? == 0 ]; then
		echo "---- Successfully installed $@"
		install_success_count=$((install_success_count+1))
	else
		echo "---- Failed to install $@"
		install_fail_count=$((install_fail_count+1))
	fi
}

remove_pkg_count=0
remove_fail_count=0
remove_success_count=0
function remove_pkg
{
	remove_pkg_count=$((remove_pkg_count+1))

	sudo apt-get -qy remove $@

	if [ $? == 0 ]; then
		echo "---- Successfully removed $@"
		remove_success_count=$((remove_success_count+1))
	else
		echo "---- Failed to remove $@"
		remove_fail_count=$((remove_fail_count+1))
	fi
}

echo "#########################################################################"
echo "#                         Fix broken packages                           #"
echo "#########################################################################"
echo ""

sudo apt-get -f install

echo ""
echo "#########################################################################"
echo "#                  Update apt cache and new packages                    #"
echo "#########################################################################"
echo ""

sudo apt-get -q update && sudo apt-get -qy upgrade

echo ""
echo "#########################################################################"
echo "#                     Remove deprecated packages                        #"
echo "#########################################################################"
echo ""

remove_pkg sendmail

echo ""
echo "#########################################################################"
echo "#                     Clean up old dependencies                         #"
echo "#########################################################################"
echo ""

sudo apt-get -qy autoremove

echo ""
echo "#########################################################################"
echo "#                      Install needed packages                          #"
echo "#########################################################################"
echo ""

install_pkg git
install_pkg gitk
install_pkg python-pip
install_pkg virtualenv
install_pkg exuberant-ctags
install_pkg vim
install_pkg ssh autossh sshpass
install_pkg nmap
install_pkg libxml2-dev
install_pkg libfcgi-dev
install_pkg tmux
install_pkg meld
install_pkg tcpdump
install_pkg wireshark-gtk
install_pkg net-tools
install_pkg avahi-daemon avahi-discover libnss-mdns avahi-utils
install_pkg gdb
install_pkg apt-file
install_pkg curl
install_pkg cmake
install_pkg check
install_pkg libczmq-dev
install_pkg ekiga
install_pkg mailutils msmtp
install_pkg enscript
install_pkg hplip
install_pkg devscripts

echo ""
echo "#########################################################################"
echo "#                               Summary                                 #"
echo "#########################################################################"
echo ""
echo "SYSTEM INFO"
echo "-----------"
echo "Date: $(date)"
echo "User: $USER"
echo "Host: $(hostname)"
echo ""
echo "INSTALLED PACKAGES"
echo "------------------"
echo "Successes: $install_success_count of $install_pkg_count"
echo "Failures: $install_fail_count of $install_pkg_count"
echo ""
echo "REMOVED PACKAGES"
echo "----------------"
echo "Successes: $remove_success_count of $remove_pkg_count"
echo "Failures: $remove_fail_count of $remove_pkg_count"

if [ $install_fail_count -gt 0 -o $remove_fail_count -gt 0 ]; then
	exit -1
fi
